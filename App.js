import React, {useEffect, useState} from 'react';
import {StyleSheet, LogBox} from 'react-native';
import {
  ViroARScene,
  ViroText,
  ViroConstants,
  ViroARSceneNavigator,
  ViroNode,
} from '@viro-community/react-viro';
import Geolocation from 'react-native-geolocation-service';
// import Geolocation from '@react-native-community/geolocation';
// Geolocation.setRNConfiguration(config);
import RNSimpleCompass from 'react-native-simple-compass';
LogBox.ignoreLogs(['new NativeEventEmitter']); // Ignore log notification by message
LogBox.ignoreAllLogs(); //Ignore all log notifications

const HelloWorldSceneAR = () => {
  // this.state = {
  //   posicionX: 0,
  //   posicionY: 0,
  // };
  var posicionX = 0;
  var posicionY = 0;
  const [objetivo_shopping, setShopping] = useState([
    {
      nombre: 'Shopping Paseo del Fuego',
      latitud: -54.7988889, //K: 0.99962639
      longitud: -68.2786111, //rad: -1.191686572381 X:(256/(2*Math.PI))*2*(-1.191686572381+Math.PI)
      altitud: 11,
      angulo: 0,
      distancia: 0,
      cateto_lat: 0,
      cateto_long: 0,
      // posX: 0,
      // posY: 0,
    },
  ]);

  const objetivo_olivia = {
    nombre: 'Monte Olivia',
    latitud: -54.7365592,
    longitud: -68.2344199,
    altitud: 941.0573408,
    angulo: 0,
    distancia: 0,
    cateto_lat: 0,
    cateto_long: 0,
    // posX: 0,
    // posY: 0,
  };

  // Los objetivos se almacenan en un array de objetivos, la distancia puede calcularse
  const objetivos = [objetivo_shopping, objetivo_olivia];

  const [ubicacion, setUbicacion] = useState([
    {latitud: 0, longitud: 0, altitud: 0},
  ]);
  const [orientacion, setOrientacion] = useState(0);
  const [text, setText] = useState('Initializing AR...');
  const [posX, setPosX] = useState(0);
  const [posY, setPosY] = useState(0);

  function onInitialized(state, reason) {
    console.log('guncelleme', state, reason);
    if (state === ViroConstants.TRACKING_NORMAL) {
      obtenerUbicacion();
      obtenerOrientacion();
      obtenerMercator();
      setText('Hello World!');
    } else if (state === ViroConstants.TRACKING_NONE) {
      // Handle loss of tracking
    }
  }

  const obtenerOrientacion = () => {
    const update_rate = 2;
    RNSimpleCompass.start((update_rate, degree) => {
      setOrientacion(degree);
      RNSimpleCompass.stop();
      return degree;
    });
  };

  //debería estar en un useeffect
  const obtenerUbicacion = () => {
    Geolocation.watchPosition(
      position => {
        setUbicacion({
          latitud: position.coords.latitude,
          longitud: position.coords.longitude,
          altitud: position.coords.altitude,
        });
      },
      error => {
        // See error code charts below.
        console.log(error.code, error.message);
      },
      {
        accuracy: 'high',
        enableHighAccuracy: true,
        distanceFilter: 1,
        maximumAge: 0,
        interval: 5000,
      },
    );
  };

  const obtenerAngulos = objetivo => {
    objetivo.angulo =
      Math.atan2(
        objetivo.longitud - ubicacion.longitud,
        objetivo.latitud - ubicacion.latitud,
      ) *
      (180 / Math.PI);
  };

  const obtenerDistancias = objetivo => {
    objetivo.distancia =
      Math.sqrt(
        Math.pow(objetivo.latitud - ubicacion.latitud, 2) +
          Math.pow(objetivo.longitud - ubicacion.longitud, 2),
        // Math.pow(altitud*1000 - ubicacion.altitud*1000, 2),
      ) * 111.1;
  };

  const obtenerCatetos = objetivo => {
    objetivo.cateto_lat = (objetivo.latitud - ubicacion.latitud) * 111.1 * 10;
    // console.log('cateto_lat: ' + objetivo.cateto_lat);
    objetivo.cateto_long =
      (objetivo.longitud - ubicacion.longitud) * 111.1 * 10;
    // console.log('cateto_long: ' + objetivo.cateto_long);
  };

  //usar variable global
  const obtenerMercator = () => {
    const mercat_shop = calcularMetros(-68.2786111, -54.7988889);

    const posicion = calcularMetros(ubicacion.longitud, ubicacion.latitud);

    let finalX = mercat_shop.x - posicion.x;
    let finalY = mercat_shop.y - posicion.y;
    // agregando ángulo
    let newRotatedX = Math.round(
      finalX * Math.cos((orientacion * Math.PI) / 180) -
        finalY * Math.sin((orientacion * Math.PI) / 180),
    );
    let newRotatedZ = Math.round(
      finalY * Math.cos((orientacion * Math.PI) / 180) +
        finalX * Math.sin((orientacion * Math.PI) / 180),
    );
    // 1ra alternativa
    // setShopping({
    //   posX: newRotatedX,
    //   posY: newRotatedZ,
    // });
    // 2da alternativa
    setPosX(newRotatedX);
    setPosY(-newRotatedZ);
    // 3ra alternativa
    // posicionX = newRotatedX;
    // posicionY = -newRotatedZ;
    console.log(newRotatedX);
    console.log(newRotatedZ);
  };

  const calcularMetros = (lon_deg, lat_deg) => {
    let lon_rad = (lon_deg / 180.0) * Math.PI;
    let lat_rad = (lat_deg / 180.0) * Math.PI;
    let ecuador = 6378137.0;
    let point = {x: 0, y: 0};
    point.x = ecuador * lon_rad;
    point.y = ecuador * Math.log((Math.sin(lat_rad) + 1) / Math.cos(lat_rad));
    // let angle = heading

    return point;
  };

  // useEffect(() => {
  //   obtenerUbicacion();
  // const obtenerMercator2 = () => {
  //   const mercat_shop = calcularMetros(-68.2786111, -54.7988889);

  //   const posicion = calcularMetros(ubicacion.longitud, ubicacion.latitud);
  //   const finalX = Math.round(mercat_shop.x - posicion.x);
  //   const finalY = -Math.round(mercat_shop.y - posicion.y);
  //   setPosX(finalX);
  //   setPosY(finalY);
  // };
  // obtenerMercator2();
  // }, [ubicacion]);

  // useEffect(() => {
  //   const mercat_shop = calcularMetros(-68.2786111, -54.7988889);
  //   const posicion = calcularMetros(ubicacion.longitud, ubicacion.latitud);
  //   const finalX = Math.round(mercat_shop.x - posicion.x);
  //   const finalY = -Math.round(mercat_shop.y - posicion.y);
  //   setPosX(finalX);
  //   setPosY(finalY);
  // }, [ubicacion]);

  useEffect(() => {
    obtenerMercator();
  });
  useEffect(() => {
    obtenerOrientacion();
    obtenerDistancias(objetivo_shopping);
  });

  return (
    <ViroARScene onTrackingUpdated={onInitialized}>
      <ViroText
        text={'X: ' + posX + ' Y: ' + posY}
        scale={[0.5, 0.5, 0.5]}
        transformBehaviors={['billboard']}
        // lo ideal sería que funcione este
        // position={[posX, 0, posY]}
        position={[0, 0, 0]}
        style={styles.helloWorldTextStyle}
      />
    </ViroARScene>
  );
};

export default () => {
  return (
    <ViroARSceneNavigator
      autofocus={true}
      initialScene={{
        scene: HelloWorldSceneAR,
      }}
      style={styles.f1}
    />
  );
};

var styles = StyleSheet.create({
  f1: {flex: 1},
  helloWorldTextStyle: {
    fontFamily: 'Arial',
    fontSize: 30,
    color: '#ffffff',
    textAlignVertical: 'center',
    textAlign: 'center',
  },
});
